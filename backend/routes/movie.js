const express = require('express')
const db = require('../db')
const util = require('../utils')

const router = express.Router()

router.get('/get/:name',(request, response)=>{
    const connection = db.createDatabaseConnection()

    const {name} = request.params

    const statement = `select * from Movie where movie_title='${name}' `

    connection.query(statement, (error, result)=>{
        connection.end()
        if(result.length>0)
        {
         response.send(util.createResult(error, result))
        }
        else
        {
         response.send('movie not found!')
        }
    })
})


router.post('/add',(request, response)=>{
    const connection = db.createDatabaseConnection()

    const {movie_title, movie_release_date, movie_time, director_name } = request.body

    const statement = `insert into Movie(movie_title, movie_release_date, movie_time,director_name) values('${movie_title}','${movie_release_date}','${movie_time}', '${director_name}')`

    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(util.createResult(error, result))
    })
})

router.put('/update/:id',(request, response)=>{
    const connection = db.createDatabaseConnection()

    const {id} = request.params

    const { movie_release_date, movie_time} = request.body

    const statement = `update Movie set movie_release_date= '${ movie_release_date}' , movie_time= '${movie_time}' where movie_id =${id}`

    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(util.createResult(error, result))
    })
})


router.delete('/delete/:id',(request, response)=>{
    const connection = db.createDatabaseConnection()

    const {id} = request.params

    const statement = `delete from Movie where movie_id =${id}`

    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(util.createResult(error, result))
    })
})


module.exports = router